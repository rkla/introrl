
from irl_maxent import gridworld as W
import maxent_lib as M
from irl_maxent import plot as P
from irl_maxent import trajectory as T
from irl_maxent import solver as S
from irl_maxent import optimizer as O

import numpy as np
import matplotlib.pyplot as plt



def print_policy(policy_2D, env):
    for y in range(policy_2D.shape[1] - 1,-1,-1):
        row_str = ""
        for x in range(policy_2D.shape[0]):
            if (x, y) in [c for c, sh in env.rbf_parameters]:
                row_str += 'C'#env.dir_strings[policy_2D[x, y]]
            elif ([x, y] == env.initial_state).all():
                row_str += 'I'
            else:
                row_str += env.dir_strings[policy_2D[x, y]]
        print(row_str)

def print_action_trajectory(traj, env):
    traj_str = ""
    for (o, a) in traj:
        traj_str += env.dir_strings[a]
    print(traj_str)

def print_state_trajectory(traj, env):
    traj_str = ""
    state_idxs = [s for (s, a) in traj]
    
    counter = 0
    for y in range(5 - 1,-1,-1):
        row_str = ""
        for x in range(5):
            if (x, y) in [c for c, sh in env.rbf_parameters]:
                row_str += 'C'#env.dir_strings[policy_2D[x, y]]
            elif ([x, y] == env.initial_state).all():
                row_str += 'I'
            elif env._get_state_idx((x, y)) in state_idxs:
                row_str += str(counter)
                counter = (counter + 1)%10
            else:
                row_str += ' '
        print(row_str)



def generate_trajectories(world, reward, initial, terminal, n_trajectories, discount, weighting, horizon):
    """
    Generate some "expert" trajectories.
    """

    # generate trajectories
    value = S.value_iteration(world.p_transition, reward, discount)
    policy = S.stochastic_policy_from_value(world, value, w=weighting)
    policy_exec = T.stochastic_policy_adapter(policy)
    tjs = list(T.generate_trajectories(n_trajectories, world, policy_exec, initial, terminal, horizon))

    return tjs, policy


def maxent(world, terminal, trajectories, features):
    """
    Maximum Entropy Inverse Reinforcement Learning
    """
    # choose our parameter initialization strategy:
    #   initialize parameters with constant
    init = O.Constant(1.0)

    # choose our optimization strategy:
    #   we select exponentiated gradient descent with linear learning-rate decay
    optim = O.ExpSga(lr=O.linear_decay(lr0=0.2))

    # actually do some inverse reinforcement learning
    reward, theta = M.irl(world.p_transition, features, terminal, trajectories, optim, init)

    return reward, theta


def maxent_causal(world, terminal_states, terminal_rewards, trajectories, features, discount=0.7):
    """
    Maximum Causal Entropy Inverse Reinforcement Learning
    """

    # choose our parameter initialization strategy:
    #   initialize parameters with constant
    init = O.Constant(1.0)

    # choose our optimization strategy:
    #   we select exponentiated gradient descent with linear learning-rate decay
    optim = O.ExpSga(lr=O.linear_decay(lr0=0.2))

    # actually do some inverse reinforcement learning
    reward, theta = M.irl_causal(world.p_transition, features, terminal_states, terminal_rewards, trajectories, optim, init, discount)

    return reward, theta

def main():
    # common style arguments for plotting
    style = {
        'border': {'color': 'red', 'linewidth': 0.5},
    }

    seed = 42
    np.random.seed(seed)

    # cirl parameters
    human_policy = "expert" # ["expert", "instructor"]
    eta = 0. # exploitation-communication tradeoff (interpolates between "expert" and "instructor")
    robot_policy = "max_causal_ent_irl" # ["max_ent_irl", "max_causal_ent_irl", "classic_irl"]

    # create our world
    size = 5
    horizon = 100
    world = W.GridWorld(size=size)
    # set terminal states
    terminal_states = []
    # set up initial probabilities (for trajectory generation)
    initial = np.zeros(world.n_states)
    initial[0] = 1.0

    # define the features
    feature_type = "rbf" # ["state", "coordinate", "rbf"]
    rbf_parameters = [((4, 4), 1.), ((1, 2), 1.), ((4, 0), 0.5)]
    if feature_type == "rbf":
        features = W.rbf_features(world, rbf_parameters)
    elif feature_type == "state":
        features = W.state_features(world)
    elif feature_type == "coordinate":
        features = W.coordinate_features(world)

    num_features = features.shape[1]

    # sample true reward parameter and reward function
    theta_bounds = [0, 1]
    theta = np.random.uniform(theta_bounds[0], theta_bounds[1], num_features)
    #theta = np.zeros(num_features)
    #theta[-1] = 1.
    true_reward = features @ theta

    # show our original reward
    ax = plt.figure(num='Original Reward').add_subplot(111)
    P.plot_state_values(ax, world, true_reward, **style)
    #plt.draw()
    print(f"theta: {theta}")

    if human_policy == "expert":
        # trajectory generation parameters
        terminal = []
        n_trajectories = 100
        discount = .3
        weighting = lambda x: np.exp(x) # maybe choosing an exponential function here is equivalent to the paper?, i.e. lambda x: np.exp(lamb*x)

        # generate "expert" trajectories
        trajectories, expert_policy = generate_trajectories(world, true_reward, initial, terminal_states, n_trajectories, discount, weighting, horizon)

        # show our expert policies
        #ax = plt.figure(num='Expert Trajectories and Policy').add_subplot(111)
        #P.plot_stochastic_policy(ax, world, expert_policy, **style)

        #for t in trajectories:
        #    P.plot_trajectory(ax, world, t, lw=5, color='white', alpha=0.025)

        #plt.draw()

    if robot_policy == "max_ent_irl":
        # maximum entropy reinforcement learning (non-causal)
        reward_maxent, theta_estimate = maxent(world, terminal_states, trajectories, features)

        # show the computed reward
        ax = plt.figure(num='MaxEnt Reward').add_subplot(111)
        P.plot_state_values(ax, world, reward_maxent, **style)
        #plt.draw()
    elif robot_policy == "max_causal_ent_irl":
        discount = .3
        terminal_rewards = true_reward#np.zeros(world.n_states)
        # maximum casal entropy reinforcement learning (non-causal)
        reward_maxcausal, theta_estimate = maxent_causal(world, terminal_states, terminal_rewards, trajectories, features, discount)

        # show the computed reward
        ax = plt.figure(num='MaxEnt Reward (Causal)').add_subplot(111)
        P.plot_state_values(ax, world, reward_maxcausal, **style)
        #plt.draw()
    elif robot_policy == "classic_irl":
        raise NotImplementedError("Coming soon TM")

    print(f"theta = {theta}")
    print(f"theta_hat: {theta_estimate}")

    # in principle the robot should then also act (but should the environment be reset in between?)
    # measure regret of R's policy: difference between the value of the policy that maximizes inferred reward and the one that maximizes ground truth reward
    # measure KL divergence between maxent trajectory distribution induced by both parameter estimates
    # measure l2 norm between vector of rewards of both estimates
    print(f"L2(theta - theta_hat) = {np.linalg.norm(theta - theta_estimate)}")

    plt.draw()
    plt.show()


if __name__ == '__main__':
    main()