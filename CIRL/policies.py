import numpy as np
from itertools import product

# TODO write code for generating instructive demonstrations
# in essence implementing equation (1) from CIRL paper
def instructive_demonstration(env, eta):
    pi_star = expert_demonstration(env)
    theta = env.true_reward_parameter

    #phi_theta = ... <- this depends on the expert policy


def maximum_entropy_irl(env, demonstrations, lamb=1.0, learning_rate=0.01, learning_rate_decay=1.0, num_gradient_steps=10, num_value_iterations=10, debug=False):
    # initialize theta estimate
    theta_hat = np.random.uniform(-env.r_max, env.r_max, size=env.num_features)#env.reward_parameter_space.sample()
    # maximize log-likelihood p(demonstrations|theta_hat)
    for ni in range(num_gradient_steps):
        # inner-loop value iteration
        q_values = value_iteration(env, theta_hat, num_iterations=num_value_iterations)
        pi_stoch = create_policy(q_values, True, lamb)
        # calculate state visitation frequencies
        p_states = state_visitation_frequencies(env, demonstrations, pi_stoch)
        # compute gradient
        gradient_theta = demonstrations.mean(axis=(0, 1)) - p_states.dot(env.features)
        # gradient descent step
        theta_hat += learning_rate*gradient_theta
        learning_rate *= learning_rate_decay
        if debug:
            print(f"Theta hat({ni}) = {theta_hat}")
    return theta_hat


def state_visitation_frequencies(env, demonstrations, pi_stoch):
    mu = np.zeros((env.horizon//2, env.num_states))
    # account for initial state distribution (here deterministic)
    mu[0, env._get_state_idx(env.initial_state)] = 1.
    # iteratively calculate visitation frequencies for all time steps
    for t in range(1, env.horizon//2):
        #mu[t, :] = np.sum([
        #            np.sum([
        #                env.transition_matrices[a][s, :]*pi_stoch[a, s]*mu[t - 1, s]
        #            for s in range(env.num_states)], axis=0)
        #            for a in range(env.num_actions)
        #            ], axis=0)
        for s, a, sp in product(range(env.num_states, env.num_actions, env.num_states)):
            mu[t, sp] += mu[t - 1, s]*pi_stoch[a, s]*env.transition_matrices[a][s, sp]
    # sum up visitation frequencies over all time steps
    svf = mu.sum(axis=0)
    return svf


def create_policy(q, stochastic=False, lamb=1.0):
    if not stochastic: 
        # obtain optimal deterministic policy
        pi_star = np.argmax(q, axis=0)
    else:
        # obtain stochastic (quasi-)optimal policy
        #q -= q.max(axis=0).reshape((1, env.num_states))
        pi_star = np.exp(lamb*q)/np.exp(lamb*q).sum(axis=0)#.reshape((1, env.num_states))
    return pi_star



def value_iteration(env, theta, num_iterations=10):
    rewards = env.features.dot(theta)
    v = np.zeros(env.num_states)
    # run value iterations
    s0 = env.initial_state
    for ni in range(num_iterations):
        v_prime_a = np.array([rewards + env.transition_matrices[a].dot(v) for a in range(env.num_actions)])
        v = np.max(v_prime_a, axis=0)
    
    # calculate q-values by one-step lookahead
    q = np.array([rewards + env.transition_matrices[a].dot(v) for a in range(env.num_actions)])
    return q
    
