from mobile_robot_env import MobileRobotWorld
from policies import instructive_demonstration, value_iteration, maximum_entropy_irl, create_policy
import numpy as np

def print_policy(policy_2D, env):
    for y in range(policy_2D.shape[1] - 1,-1,-1):
        row_str = ""
        for x in range(policy_2D.shape[0]):
            if (x, y) in [c for c, sh in env.rbf_parameters]:
                row_str += 'C'#env.dir_strings[policy_2D[x, y]]
            elif ([x, y] == env.initial_state).all():
                row_str += 'I'
            else:
                row_str += env.dir_strings[policy_2D[x, y]]
        print(row_str)

def print_action_trajectory(traj, env):
    traj_str = ""
    for (o, a) in traj:
        traj_str += env.dir_strings[a]
    print(traj_str)

def print_state_trajectory(traj, env):
    traj_str = ""
    state_idxs = [s for (s, a) in traj]
    
    counter = 0
    for y in range(5 - 1,-1,-1):
        row_str = ""
        for x in range(5):
            if (x, y) in [c for c, sh in env.rbf_parameters]:
                row_str += 'C'#env.dir_strings[policy_2D[x, y]]
            elif ([x, y] == env.initial_state).all():
                row_str += 'I'
            elif env._get_state_idx((x, y)) in state_idxs:
                row_str += str(counter)
                counter = (counter + 1)%10
            else:
                row_str += ' '
        print(row_str)



# setup environment
seed = 43
size = (5, 5)
horizon = 20
rbf_parameters = [((4, 4), 1.), ((1, 2), 1.)]
env = MobileRobotWorld(size, horizon, rbf_parameters)

# seed the environment (also samples a true reward parameter)
env.seed(seed)

print("Horizon: ", env.horizon)
print("Initial state: ", env.initial_state)
print("Theta star: ", env.true_reward_parameter)

# choose human policy (either "expert" or "best_response")
human_policy = "expert"
stochastic = True
lamb_human = 1.0
eta = 1. # exploitation-communication tradeoff
num_iterations = 10
num_demonstrations = 1000

if human_policy == "expert":
    # use MDP solver to obtain optimal demonstration
    # can we assume that human knows dynamics? probably yes -> use DP
    q_values = value_iteration(env, env.true_reward_parameter, num_iterations) 
    policy = create_policy(q_values, stochastic, lamb=lamb_human)

    human_returns = []
    trajectories = [[] for _ in range(num_demonstrations)]
    demonstrations = np.zeros((num_demonstrations, env.horizon//2, env.num_features))
    for nd in range(num_demonstrations):
        human_return = 0
        done = False
        obs = env.reset()
        while not done:
            state_idx = env._get_state_idx()
            if stochastic:
                a = np.random.choice(env.num_actions, p=policy[:, state_idx])
            else:
                a = policy[state_idx]
            trajectories[nd].append((state_idx, a))
            demonstrations[nd, env.time_step, :] = obs
            obs, reward, done, _ = env.step(a)
            human_return += reward
        human_returns.append(human_return)

        print(f"Expert action trajectory {nd}: ")
        print_action_trajectory(trajectories[nd], env)
        print(f"Expert state trajectory {nd}: ")
        print_state_trajectory(trajectories[nd], env)
    if not stochastic:
        print("Expert policy:")
        policy_2D = policy.reshape(*size).T
        print_policy(policy_2D, env)

elif human_policy == "best_response":
    action_trajectory = instructive_demonstration(env)


# learn about reward from the human demonstration
lamb = 1. # MaxEnt IRL parameter -> what is it? doesn't appear in Ziebart 2008
lr = 0.01
lr_decay = 0.995
num_grad_steps = 1000
num_val_iters = 10
inferred_reward_parameter = maximum_entropy_irl(env, demonstrations, lamb, lr, lr_decay, num_grad_steps, num_val_iters, debug=True)
print(f"Theta hat = {inferred_reward_parameter}")
print("Theta star: ", env.true_reward_parameter)

# in principle the robot should then also act (but should the environment be reset in between?)
# measure regret of R's policy: difference between the value of the policy that maximizes inferred reward and the one that maximizes ground truth reward
# measure KL divergence between maxent trajectory distribution induced by both parameter estimates
# measure l2 norm between vector of rewards of both estimates
print(env.true_reward_parameter / inferred_reward_parameter)
print(f"L2(theta, theta_hat) = {np.linalg.norm(env.true_reward_parameter - inferred_reward_parameter)}")

import matplotlib.pyplot as plt
plt.imshow(env.features.dot(inferred_reward_parameter).reshape(5, 5))
plt.colorbar()
plt.show()