import numpy as np
from scipy.spatial.distance import cdist

import gym
from gym.spaces import MultiDiscrete, Discrete, Box

# define Mobile Robot environment used in the original CIRL paper
# Note: this is not a MDP since there are two players and partial information, still the Gym specification is useful
class MobileRobotWorld(gym.Env):
    def __init__(self, size=(5, 5), horizon=10, rbf_parameters=[((4, 4), 1.), ((1, 2), 1.)]):
        self.size = size
        self.horizon = horizon 
        self.initial_state = np.array(self.size)//2
        self.agent_state = self.initial_state.copy()
        
        # calculate coordinates of all grid points
        points_1D = np.zeros((np.prod(self.size), 2)) 
        points_idx = 0
        for x in range(self.size[0]):
            for y in range(self.size[1]):
                points_1D[points_idx, :] = np.array([x, y])
                points_idx += 1

        # define state space
        self.state_space = MultiDiscrete(list(self.size))
        self.num_states = np.prod(self.size)

        # define action space (this is symmetrical since the human teleoperates the robot in the learning phase)
        self.dir_strings = ["^", ">", "v", "<", "_"]
        self.directions = [np.array((0, +1)), np.array((+1, 0)), np.array((0, -1)), np.array((-1, 0)), np.array((0, 0))]
        self.dir_map = dict(zip(self.dir_strings, self.directions))
        self.action_space = Discrete(len(self.directions))
        self.num_actions = len(self.directions)

        # define transition dynamics
        self.transition_matrices = []
        for a in range(len(self.directions)):
            transition_matrix = np.zeros((self.num_states, self.num_states))
            for state_from_idx in range(self.num_states):
                state_from = self._get_state(state_from_idx)
                state_from_legal_actions = self._get_legal_actions(state_from)
                # if action is legal in this state it succeeds with probability 1
                if a in state_from_legal_actions:
                    # simulate step
                    state_to = state_from + self.directions[a]
                    transition_matrix[state_from_idx, self._get_state_idx(state_to)] = 1.
            self.transition_matrices.append(transition_matrix)

        # define the features
        self.num_features = len(rbf_parameters)
        self.rbf_parameters = rbf_parameters
        self.features = np.zeros((self.num_states, self.num_features))
        for fi, rbf in enumerate(self.rbf_parameters):
            center, shape = rbf
            dists = cdist(np.array(center)[None, :], points_1D, metric="euclidean")
            self.features[:, fi] = np.exp(-shape*dists**2)

        # define reward space
        self.r_max = 1
        self.reward_parameter_space = Box(-self.r_max, self.r_max, shape=(self.num_features,))
        self.true_reward_parameter = self.reward_parameter_space.sample()
        
        # track time
        self.time_step = 0

    def seed(self, seed):
        if seed is not None:
            self.reward_parameter_space.seed(seed)
            self.action_space.seed(seed)
            self.state_space.seed(seed)
        
        # reset true reward parameter
        self.true_reward_parameter = self.reward_parameter_space.sample()

    def reset(self):
        self.time_step = 0
        # reset the agent position
        self.agent_state = self.initial_state.copy()
        return self.agent_state

    def step(self, action, reward_parameter=None):
        reward = 0
        terminal = False
        
        # execute action
        state = self._get_state()
        legal_actions = self._get_legal_actions(state)
        if action in legal_actions:
            self.agent_state += self.directions[action]
        new_state = self._get_state()

        # get features for this state 
        features = self.features[self._get_state_idx()]

        # calculate reward
        if reward_parameter is None:
            reward = features.T.dot(self.true_reward_parameter)
        else:
            reward = features.T.dot(reward_parameter)

        # end epsisode if horizon is reached
        self.time_step += 1
        if self.time_step == self.horizon//2 or self.time_step == self.horizon:
            terminal = True
        return features, reward, terminal, {}
    
    def _get_legal_actions(self, state):
        legal_actions = []
        for a in range(len(self.directions)):
            next_state = state + self.directions[a]
            if next_state[0] >= 0 and next_state[0] < self.size[0] and next_state[1] >= 0 and next_state[1] < self.size[1]:
                legal_actions.append(a)
        return legal_actions

    def _get_state(self, idx=None):
        if idx is None:
            return self.agent_state
        else:
            return np.array((idx % self.size[0], idx // self.size[0]))
    
    def _get_obs(self):
        return self.features[self._get_state_idx(),:]

    def _get_state_idx(self, state=None):
        if state is None:
            return self.agent_state[1]*self.size[0] + self.agent_state[0]
        else:
            return state[1]*self.size[0] + state[0]

    def render(self):
        print("-"*self.size[0])
        for y in range(self.size[1] - 1, -1, -1):
            row_str = ""
            for x in range(self.size[0]):
                if (np.array((x, y)) == self.agent_state).all():
                    row_str += "A"
                elif ((x, y) in [c for c, sh in self.rbf_parameters]).all():
                    row_str += "C"
                else:
                    row_str += " "
            print(row_str)
        print("-"*self.size[0])

    def close(self):
        pass
    
