import numpy as np
import matplotlib.pyplot as plt
from gym import Env
from gym.spaces import MultiDiscrete, Discrete

# define MDP
class GridWorld(Env):
    def __init__(self, size=(5, 5), goal_state=(4, 4), initial_state=(0, 0), p_random=0.3, gamma=1.):
        self.size = size
        self.gamma = gamma
        self.p_random = p_random
        self.goal_state = np.array(goal_state)
        self.initial_state = np.array(initial_state)
        self.agent_state = initial_state

        # define observation space
        self.observation_space = MultiDiscrete(list(self.size))
        self.num_observations = np.prod(self.size)

        # define action space
        self.dir_strings = ["N", "E", "S", "W"]
        self.directions = [np.array((0, +1)), np.array((+1, 0)), np.array((0, -1)), np.array((-1, 0))]
        self.dir_map = dict(zip(self.dir_strings, self.directions))
        self.action_space = Discrete(len(self.directions))
        self.num_actions = len(self.directions)

        # define transition dynamics
        self.transition_matrices = []
        for a in range(len(self.directions)):
            transition_matrix = np.zeros((self.num_observations, self.num_observations))
            for obs_from_idx in range(self.num_observations):
                obs_from = self._get_obs(obs_from_idx)
                obs_from_legal_actions = self._get_legal_actions(obs_from)
                # if action is legal in this state it succeeds wit 1 - p_random
                if a in obs_from_legal_actions:
                    # simulate step
                    obs_to = obs_from + self.directions[a]
                    transition_matrix[obs_from_idx, self._get_obs_idx(obs_to)] += 1 - self.p_random
                    # a random action is drawn adding p=p_random/len(legal_actions) to each possible outcome
                    for random_a in obs_from_legal_actions:
                        obs_to = obs_from + self.directions[random_a]
                        transition_matrix[obs_from_idx, self._get_obs_idx(obs_to)] += self.p_random/len(obs_from_legal_actions)
                # a random action is drawn adding p=1/len(legal_actions) to each possible outcome
                else:
                    for random_a in obs_from_legal_actions:
                        obs_to = obs_from + self.directions[random_a]
                        transition_matrix[obs_from_idx, self._get_obs_idx(obs_to)] += 1/len(obs_from_legal_actions)
            self.transition_matrices.append(transition_matrix)

        # define reward function
        self.reward_function = np.zeros(self.num_observations)
        self.reward_function[self._get_obs_idx(self.goal_state)] = 1
        self.r_max = 1

        # define optimal policy
        def optimal_policy_function2(state):
            in_bottom_tri = (self.size[0] - state[0] > state[1] + 1 and state[0] >= state[1])
            in_top_tri = (self.size[0] - state[0] < state[1] + 2 and state[0] < state[1])
            if in_bottom_tri or in_top_tri:
                return 1
            else:
                return 0

        def optimal_policy_function(state):
            idx = self._get_obs_idx(state)
            lookup = [0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1]
            return lookup[idx]

        self.optimal_policy = optimal_policy_function
        print("Optimal policy")
        for y in range(self.size[1] - 1, -1, -1):
            row_str = ""
            for x in range(self.size[0]):
                row_str += self.dir_strings[self.optimal_policy((x, y))]
            print(row_str)

    def reset(self):
        # reset the agent position
        self.agent_state = self.initial_state
        return self.agent_state

    def step(self, action):
        reward = 0
        terminal = False
        obs = self._get_obs()
        legal_actions = self._get_legal_actions(obs)
        # take random action with some probability (or if the chosen action is not legal)
        if np.random.random() < self.p_random or action not in legal_actions:
            action_idx = np.random.randint(len(legal_actions))
            self.agent_state += self.directions[legal_actions[action_idx]]
        else: # take the chosen action
            self.agent_state += self.directions[action]
        new_obs = self._get_obs()
        # end epsisode if goal is reached
        if (obs == self.goal_state).all():
            reward = +1
            terminal = True
        return new_obs, reward, terminal, {}

    def _get_legal_actions(self, state):
        legal_actions = []
        for a in range(len(self.directions)):
            next_state = state + self.directions[a]
            if next_state[0] >= 0 and next_state[0] < self.size[0] and next_state[1] >= 0 and next_state[1] < self.size[1]:
                legal_actions.append(a)
        return legal_actions

    def _get_obs(self, idx=None):
        if idx is None:
            return self.agent_state
        else:
            return np.array((idx % self.size[0], idx // self.size[0]))

    def _get_obs_idx(self, obs=None):
        if obs is None:
            return self.agent_state[1]*self.size[0] + self.agent_state[0]
        else:
            return obs[1]*self.size[0] + obs[0]

    def render(self):
        print("-"*self.size[0])
        for y in range(self.size[1] - 1, -1, -1):
            row_str = ""
            for x in range(self.size[0]):
                if (np.array((x, y)) == self.agent_state).all():
                    row_str += "A"
                elif (np.array((x, y)) == self.goal_state).all():
                    row_str += "G"
                else:
                    row_str += " "
            print(row_str)
        print("-"*self.size[0])

    def close(self):
        pass

def irl_finite(env, use_scip=False):
    # number of states
    S = env.num_observations
    A = env.num_actions

    # regularization
    lamb = 0.

    # calculate dot_term(state, action)
    Cs = {}
    A_terms = []
    I_terms = []
    for s in range(S):
        state = env._get_obs(s)
        opt_action = env.optimal_policy(state)
        p_opt = env.transition_matrices[opt_action]
        for a in set(range(A)) - {opt_action}:
            p_a = env.transition_matrices[a]
            inv_one_minus_p_opt = np.linalg.inv(1 - env.gamma*p_opt)
            dot_term = (p_opt[s, :] - p_a[s, :]).dot(inv_one_minus_p_opt)
            Cs[(s, a)] = -dot_term
            A_terms.append(-dot_term)
            I_terms.append(np.eye(1, S, s))

    if not use_scip:
        # use Scipy to solve linear program

        # define objective vector
        c = np.hstack((np.zeros(S), np.ones(S), -lamb*np.ones(S)))

        # define constraint matrix of shape (2*S*(A - 1) + 2*S, 3*S)
        A_ub = np.zeros((2*S*(A - 1) + 2*S, 3*S))

        A_ub[:S*(A - 1), :S] = np.vstack(A_terms)
        A_ub[S*(A - 1):2*S*(A-1), :S] = np.vstack(A_terms)
        A_ub[2*S*(A-1):2*S*(A-1) + S, :S] = -np.eye(S)
        A_ub[2*S*(A-1) + S:, :S] = np.eye(S)

        A_ub[:S*(A - 1), S:2*S] = np.vstack(I_terms)
        A_ub[2*S*(A-1):2*S*(A-1) + S, 2*S:] = -np.eye(S)
        A_ub[2*S*(A-1) + S:, 2*S:] = -np.eye(S)

        b = np.zeros((2*S*(A - 1) + 2*S, 1))

        # bounding R and regularization variables
        bounds = [(-env.r_max, env.r_max)]*S + [(None, None)]*S + [(-env.r_max, env.r_max)]*S

        # solve the problem
        from scipy.optimize import linprog
        res = linprog(-c, A_ub=A_ub, b_ub=b, bounds=bounds, method="simplex")
        dist_to_true = np.linalg.norm(res["x"][:S] - env.reward_function)
        print(f"Distance to true reward: {dist_to_true}")

        # plot result
        r_2d = np.zeros(env.size)
        for i in range(env.num_observations):
            obs = env._get_obs(i)
            r_2d[obs[0], obs[1]] = res["x"][i]

        plt.figure()
        ax = plt.axes(projection='3d')
        y = np.arange(env.size[0])
        x = np.arange(env.size[1])
        X, Y = np.meshgrid(x, y)
        ax.contour3D(X, Y, r_2d, 100)
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("reconstructed reward function")
        plt.show()
    else:

        # define SCIP Model
        from pyscipopt import Model, quicksum
        scip_model = Model("IRL LP")

        # define variables
        Y = {}
        R = {}
        w = {}
        for s in range(S):
            Y[s] = scip_model.addVar(vtype="C", lb=0.)
            R[s] = scip_model.addVar(vtype="C", lb=-env.r_max, ub=env.r_max)
            w[s] = scip_model.addVar(vtype="C", lb=-env.r_max, ub=env.r_max)

        # add constraints
        for s in range(S):
            scip_model.addCons(R[s] - w[s] <= 0)
            scip_model.addCons(-w[s] - R[s] <= 0)
            state = env._get_obs(s)
            opt_action = env.optimal_policy(state)
            for a in set(range(A)) - set([opt_action]):
                scip_model.addCons(-quicksum(Cs[s, a][s_prime]*R[s_prime] for s_prime in range(S)) + Y[s] <= 0.)
                scip_model.addCons(-quicksum(Cs[s, a][s_prime]*R[s_prime] for s_prime in range(S)) <= 0.)

        # define the objective
        scip_model.setObjective(quicksum(Y[s] for s in range(S)) - lamb*quicksum(w[s] for s in range(S)), "maximize")

        # solve the problem
        scip_model.optimize()

        # extract best solution
        scip_solution = scip_model.getBestSol()
        R_best = []

        for s in range(S):
            R_best.append(scip_solution[R[s]]) # extract value of a variable
        print(f"Distance to true reward: {np.linalg.norm(np.array(R_best) - env.reward_function)}")

        # plot results
        r_2d = np.zeros(env.size)
        for i in range(env.num_observations):
            obs = env._get_obs(i)
            r_2d[obs[0], obs[1]] = R_best[i]

        plt.figure()
        ax = plt.axes(projection='3d')
        y = np.arange(env.size[0])
        x = np.arange(env.size[1])
        X, Y = np.meshgrid(x, y)
        ax.contour3D(X, Y, r_2d, 100)
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("reward function")
        plt.show()

if __name__ == "__main__":
    env = GridWorld()
    irl_finite(env, use_scip=False)