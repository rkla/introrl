import easy21_env
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

# Results
# average return for Sarsa(lambda) policy = -0.563 +- 0.8222110434675516 @ 1.000E+03 episodes

def epsilon_greedy(action_values, epsilon):
    if np.random.random() < epsilon: # take a random action
        action = np.random.randint(0, 2)
    else: # take a greedy action
        action = np.random.choice(np.flatnonzero(action_values == action_values.max()))
    return action

if __name__ == "__main__":
    # load "true" action-values from MC control
    Q_MC = np.load("Easy21/results/MC_Q_values_1000000ep.npy")

    # parameters
    verbose = False
    lambdas = np.arange(0, 1.1, 0.1)
    N0 = 100
    num_episodes = 10000

    # containers
    mean_squared_errors = []
    mean_squared_errors_lambda0 = []
    mean_squared_errors_lambda1 = []
    return_episode = np.zeros(num_episodes)

    for lamb in lambdas:

        Q = np.zeros((10, 21, 2))
        N = np.zeros_like(Q)
        epsilon = lambda st: N0/(N0 + N[st[0], st[1], :].sum())
        alpha = lambda st, a: 1/N[st[0], st[1], a]

        for ep in range(num_episodes):
            terminated = False
            G_ep = 0
            E = np.zeros_like(Q)
            state_action_pairs = []

            # initialize game
            easygame = easy21_env.Easy21()
            state = easygame.reset()

            # sample first action
            action = epsilon_greedy(Q[state[0], state[1], :], epsilon(state))

            if verbose:  
                print(f"Game {ep}")

            # sample an episode
            while not terminated:

                if verbose:
                    print(f"Player decides to {'hit' if action == 0 else 'stick'}")

                # environment step
                state_prime, reward = easygame.step(state, action)

                if verbose:
                    if state is easygame.TERMINAL:
                        if reward == 1: print("Player won.")
                        elif reward == 0: print("Draw.")
                        elif reward == -1: print("Dealer won.")
                    else:
                        print(f"Dealer's first card: {state[0]}, player's sum: {state[1]}")

                # calculate TD error
                if not state_prime is easygame.TERMINAL:
                    action_prime = epsilon_greedy(Q[state_prime[0], state_prime[1], :], epsilon(state_prime))
                    td_error = reward + Q[state_prime[0], state_prime[0], action_prime] - Q[state[0], state[1], action]
                else:
                    td_error = reward - Q[state[0], state[1], action]

                # update counts, eligibility and cache state-action pair
                N[state[0], state[1], action] += 1
                E[state[0], state[1], action] += 1
                state_action_pairs.append((state, action))

                for (state_, action_) in state_action_pairs:
                    Q[state_[0], state_[1], action_] += alpha(state_, action_) * td_error * E[state_[0], state_[1], action_]
                    E[state_[0], state_[1], action_] *= lamb

                if not state_prime is easygame.TERMINAL:
                    state, action = state_prime, action_prime
                else:
                    terminated = True

                G_ep += reward
        
            # store episode return
            return_episode[ep] = G_ep

            # for lambda=0 and =1 save mean squared error per episode
            if lamb == 0.:
                mean_squared_errors_lambda0.append(np.sum((Q - Q_MC)**2))
            if lamb == 1.:
                mean_squared_errors_lambda1.append(np.sum((Q - Q_MC)**2))

        # calculate mean squared error for each lambda
        mean_squared_error_Q = np.sum((Q - Q_MC)**2)
        print(f"Mean squared error sum(Q - Q*)^2 = {mean_squared_error_Q}")
        mean_squared_errors.append(mean_squared_error_Q)
    print(f"average return for Sarsa(lambda) policy = {return_episode.mean()} +- {return_episode.std()} @ {num_episodes:.3E} episodes")

    plt.figure()
    plt.plot(lambdas, mean_squared_errors)
    plt.xlabel("lambda")
    plt.ylabel("MSE = sum(Q-Q*)^2")
    plt.grid()
    plt.show()

    plt.figure()
    plt.plot(range(num_episodes), mean_squared_errors_lambda0, label="lambda=0")
    plt.plot(range(num_episodes), mean_squared_errors_lambda1, label="lambda=1")
    plt.xlabel("episode")
    plt.ylabel("MSE = sum(Q-Q*)^2")
    plt.grid()
    plt.legend()
    plt.show()