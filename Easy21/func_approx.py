import easy21_env
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

# Results
# average return for Sarsa(lambda) policy = -0.577 +- 0.8124475367677597 @ 1.000E+03 episodes

def epsilon_greedy(action_values, epsilon):
    if np.random.random() < epsilon: # take a random action
        action = np.random.randint(0, 2)
    else: # take a greedy action
        action = np.random.choice(np.flatnonzero(action_values == action_values.max()))
    return action

def coarse_coding_function(state, action):
    # detect tiles
    dealer_tiles = [range(0, 4), range(3, 7), range(6, 10)]
    player_tiles = [range(0, 6), range(3, 9), range(6, 12), range(9, 15), range(12, 18), range(15, 21)]
    action_tiles = [[0], [1]]

    dealer_index, player_index, action_index = None, None, None
    for di, dealer_tile in enumerate(dealer_tiles):
        if state[0] in dealer_tile:
            dealer_index = di
    for pi, player_tile in enumerate(player_tiles):
        if state[1] in player_tile:
            player_index = pi
    for ai, action_tile in enumerate(action_tiles):
        if action in action_tile:
            action_index = ai

    # construct feature vector
    phi = np.zeros(len(dealer_tiles)*len(player_tiles)*len(action_tiles))
    phi_index = dealer_index + player_index*len(dealer_tiles) + action_index*len(dealer_tiles)*len(player_tiles)
    phi[phi_index] = 1
    return phi

def Q(state, action, theta):
    return coarse_coding_function(state, action).dot(theta)

def calc_all_Q(theta):
    Q_values = np.zeros((10, 21, 2))
    for di in range(10):
        for pi in range(21):
            for ai in range(2):
                Q_values[di, pi, ai] = Q((di, pi), ai, theta)
    return Q_values

if __name__ == "__main__":
    # load "true" action-values from MC control
    Q_MC = np.load("Easy21/results/MC_Q_values_1000000ep.npy")

    # parameters
    verbose = True
    lambdas = np.arange(0, 1.1, 0.1)
    N0 = 100
    num_episodes = 1000

    # containers
    mean_squared_errors = []
    mean_squared_errors_lambda0 = []
    mean_squared_errors_lambda1 = []
    return_episode = np.zeros(num_episodes)

    for lamb in lambdas:

        theta = np.zeros(3*6*2)
        epsilon = 0.05
        alpha = 0.01

        for ep in range(num_episodes):
            terminated = False
            G_ep = 0
            E = np.zeros_like(theta)
            state_action_pairs = []

            # initialize game
            easygame = easy21_env.Easy21()
            state = easygame.reset()

            # sample first action
            action_values = np.array([Q(state, a, theta) for a in range(2)])
            action = epsilon_greedy(action_values, epsilon)

            if verbose:  
                print(f"Game {ep}")

            # sample an episode
            while not terminated:

                if verbose:
                    print(f"Player decides to {'hit' if action == 0 else 'stick'}")

                # environment step
                state_prime, reward = easygame.step(state, action)

                if verbose:
                    if state is easygame.TERMINAL:
                        if reward == 1: print("Player won.")
                        elif reward == 0: print("Draw.")
                        elif reward == -1: print("Dealer won.")
                    else:
                        print(f"Dealer's first card: {state[0]}, player's sum: {state[1]}")


                # calculate TD error
                if not state_prime is easygame.TERMINAL:
                    action_values = np.array([Q(state, a, theta) for a in range(2)])
                    action_prime = epsilon_greedy(action_values, epsilon)
                    td_error = reward + Q(state_prime, action_prime, theta) - Q(state, action, theta)
                else:
                    td_error = reward - Q(state, action, theta)

                # update weight vector and eligibility trace
                feature = coarse_coding_function(state, action)
                gradient = alpha * td_error * E * feature
                theta += gradient
                E = lamb*E + feature

                if not state_prime is easygame.TERMINAL:
                    state, action = state_prime, action_prime
                else:
                    terminated = True

                G_ep += reward
        
            # store episode return
            return_episode[ep] = G_ep

            # for lambda=0 and =1 save mean squared error per episode
            if lamb == 0.:
                mean_squared_errors_lambda0.append(np.sum((calc_all_Q(theta) - Q_MC)**2))
            if lamb == 1.:
                mean_squared_errors_lambda1.append(np.sum((calc_all_Q(theta) - Q_MC)**2))

        # calculate mean squared error for each lambda
        mean_squared_error_Q = np.sum((calc_all_Q(theta) - Q_MC)**2)
        print(f"Mean squared error sum(Q - Q*)^2 = {mean_squared_error_Q}")
        mean_squared_errors.append(mean_squared_error_Q)

    print(f"average return for Sarsa(lambda) policy = {return_episode.mean()} +- {return_episode.std()} @ {num_episodes:.3E} episodes")

    # plot MSE w.r.t to MC control
    plt.figure()
    plt.plot(lambdas, mean_squared_errors)
    plt.xlabel("lambda")
    plt.ylabel("MSE = sum(Q-Q*)^2")
    plt.grid()
    plt.show()

    plt.figure()
    plt.plot(range(num_episodes), mean_squared_errors_lambda0, label="lambda=0")
    plt.plot(range(num_episodes), mean_squared_errors_lambda1, label="lambda=1")
    plt.xlabel("episode")
    plt.ylabel("MSE = sum(Q-Q*)^2")
    plt.grid()
    plt.legend()
    plt.show()