import easy21_env
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

# Results
# average return of random policy = -0.6 +- 0.8
# average return for MC control policy = -0.51234 +- 0.8505573022436523 @ 100k episodes
# average return for MC control policy = -0.457505 +- 0.8736212995199922 @ 1.000E+06 episodes

def epsilon_greedy(action_values, epsilon):
    if np.random.random() < epsilon: # take a random action
        action = np.random.randint(0, 2)
    else: # take a greedy action
        action = np.random.choice(np.flatnonzero(action_values == action_values.max()))
    return action

if __name__ == "__main__":
    # parameters
    verbose = True
    gamma = 1
    N0 = 100
    num_episodes = 1000000

    Q = np.zeros((10, 21, 2))
    N = np.zeros_like(Q)
    epsilon = lambda st: N0/(N0 + N[st[0], st[1], :].sum())
    alpha = lambda st, a: 1/N[st[0], st[1], a]
    return_episode = np.zeros(num_episodes)

    for ep in range(num_episodes):
        terminated = False
        G_ep = 0
        visited_state_action_pairs = []

        # initialize game
        easygame = easy21_env.Easy21()
        state = easygame.reset()
   
        if verbose:
            print(f"Game {ep}:")
            print(f"Dealer's first card: {state[0]}, player's sum: {state[1]}")

        # Generate episode  
        while not terminated:
            # sample action from current epsilon-greedy policy
            # action = np.random.randint(0, 2) # random policy
            action = epsilon_greedy(Q[state[0], state[1], :], epsilon(state))

            # update counts and cache state-action pair
            N[state[0], state[1], action] += 1
            visited_state_action_pairs.append((state, action))

            if verbose:
                print(f"Player decides to {'hit' if action == 0 else 'stick'}")

            # environment step
            state, reward = easygame.step(state, action)

            if verbose:
                if state is easygame.TERMINAL:
                    if reward == 1: print("Player won.")
                    elif reward == 0: print("Draw.")
                    elif reward == -1: print("Dealer won.")
                else:
                    print(f"Dealer's first card: {state[0]}, player's sum: {state[1]}")      

            # calculate return
            G_ep += reward

            if state is easygame.TERMINAL:
                terminated = True

        # update Q values of the visited state-action pairs
        for (state, action) in visited_state_action_pairs:
            Q[state[0], state[1], action] += alpha(state, action)*(G_ep - Q[state[0], state[1], action])
        return_episode[ep] = G_ep

    print(f"average return for MC control policy = {return_episode.mean()} +- {return_episode.std()} @ {num_episodes:.3E} episodes")
    
    # save the Q values
    #np.save(os.path.join("Easy21", "results", f"MC_Q_values_{num_episodes}ep.npy"), Q)

    # calculate optimal value function
    V_opt = np.max(Q, axis=2)

    # plot optimal value function
    cmap = mpl.cm.get_cmap("seismic")
    plt.figure()
    plt.title("Optimal value function")
    maxabs = np.abs(V_opt).max()
    plt.imshow(V_opt, vmin=-maxabs, vmax=maxabs, cmap=cmap)
    plt.ylabel("dealer showing")
    plt.xlabel("player sum")
    plt.colorbar()
    #plt.savefig(os.path.join("Easy21", "results", f"MC_state_values_opt_{num_episodes}ep.png"))

    plt.figure()
    ax = plt.axes(projection='3d')
    y_dealer = np.arange(10)
    x_player = np.arange(21)
    X, Y = np.meshgrid(x_player, y_dealer)
    ax.contour3D(X, Y, V_opt, 100)
    ax.set_xlabel("player sum")
    ax.set_ylabel("dealer showing")
    #plt.savefig(os.path.join("Easy21", "results", f"MC_state_values_opt_3D_{num_episodes}ep.png"))
    
    if verbose:
        plt.show()