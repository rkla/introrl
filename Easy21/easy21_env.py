import random

class Easy21:
    # ACTIONS
    HIT = 0
    STICK = 1

    # COLOR
    RED = -1
    BLACK = 1

    # TERMINAL STATE
    TERMINAL = (None, None)

    # ENVIRONMENT STATE
    dealer_card1 = None

    def draw_card(self, color=None):
        """ Draws a card """
        value = random.randint(1, 10)
        if color is None:
            color = self.RED if random.random() <= 1/3 else self.BLACK
        return value*color

    def reset(self):
        """ Resets the environment and returns the initial state """
        self.dealer_card1 = self.draw_card(self.BLACK)
        player_card1 = self.draw_card(self.BLACK)
        initial_state = (self.dealer_card1 - 1, player_card1 - 1)
        return initial_state

    def step(self, state, action):
        """ Takes one action and returns the next state and the reward """
        dealer_first_card, player_sum = state
        dealer_first_card += 1
        player_sum += 1
        if action == self.STICK:
            # simulate the dealer
            dealer_sum = self.dealer_card1
            while dealer_sum < 17:
                new_dealer_card = self.draw_card()
                dealer_sum += new_dealer_card
            # end of the game
            if dealer_sum > 21: # player wins
                return self.TERMINAL, +1
            if player_sum > dealer_sum: # player wins
                reward = +1
            elif player_sum == dealer_sum: # draw
                reward = 0
            elif player_sum < dealer_sum: # dealer wins
                reward = -1
            return self.TERMINAL, reward
        elif action == self.HIT:
            new_player_card = self.draw_card()
            player_sum += new_player_card
            if player_sum > 21 or player_sum < 1: # GOES BUST
                next_state = self.TERMINAL
                reward = -1
            else: # CONTINUES
                next_state = (dealer_first_card - 1, player_sum - 1)
                reward = 0
            return next_state, reward
        else:
            raise ValueError(f"{action} is no valid action.")
